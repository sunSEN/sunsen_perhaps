package com.hp.perhaps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

/**
 * Класс для работы activity_complain.xml (основная xml)
 * 
 * @author hp
 * 
 */
public class ComplainActivity extends Activity implements OnClickListener {

	ImageButton btnMain;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_complain);

		btnMain = (ImageButton) findViewById(R.id.btnMain);
		btnMain.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_complain, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		switch (item.getItemId()) {

		case R.id.level:
			startActivity(new Intent(this, LevelActivity.class));
			return true;

			// case R.id.play:
			// Toast.makeText(this, "Go animation", Toast.LENGTH_LONG).show();
			// return true;

		case R.id.question:
			Toast.makeText(this, "Все будет хорошо =)", Toast.LENGTH_LONG)
					.show();
			return true;

		case R.id.about:
			startActivity(new Intent(this, AboutActivity.class));
			return true;

		default:
			return super.onMenuItemSelected(featureId, item);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnMain:
			startActivity(new Intent(ComplainActivity.this,
					PerhapsActivity.class));
			break;
		}

	}
}
