package com.hp.perhaps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

public class LevelActivity extends Activity implements OnCheckedChangeListener,
		OnClickListener {

	/**
	 * Уровень сложности.
	 * 
	 * @author hp
	 * 
	 */
	public enum LevelType {

		LEVEL_LIGHT, LEVEL_NORMAL, LEVEL_HARD
	}

	private RadioGroup mRgroup;
	private LevelType mLevelType;
	private ImageButton mBtnOk;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_level);

		mRgroup = (RadioGroup) findViewById(R.id.rg_level);
		mRgroup.setOnCheckedChangeListener(this);
		mBtnOk = (ImageButton) findViewById(R.id.btnOk);
		mBtnOk.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_complain, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		switch (item.getItemId()) {

		// case R.id.play:
		// Toast.makeText(this, "Go animation", Toast.LENGTH_LONG).show();
		// return true;

		case R.id.question:
			Toast.makeText(this, "Все будет хорошо =)", Toast.LENGTH_LONG)
					.show();
			return true;

		case R.id.about:
			startActivity(new Intent(this, AboutActivity.class));
			return true;

		default:
			return super.onMenuItemSelected(featureId, item);
		}
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (checkedId) {

		case R.id.rbtnLight:
			mLevelType = LevelType.LEVEL_LIGHT;
			break;

		case R.id.rbtnNormal:
			mLevelType = LevelType.LEVEL_NORMAL;
			break;

		case R.id.rbtnHard:
			mLevelType = LevelType.LEVEL_HARD;
			break;

		}
		// Toast.makeText(this, "Выбран " + mLevelType.name(),
		// Toast.LENGTH_SHORT)
		// .show();
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.btnOk:
			if (mLevelType != null) {
				startActivity(getNextActivityIntent());
			} else {
				Toast.makeText(this, "Выбери уровень сложности",
						Toast.LENGTH_SHORT).show();
			}
			break;
		}

	}

	private Intent getNextActivityIntent() {
		Intent nextActivityIntent = null;

		switch (mLevelType) {
		case LEVEL_LIGHT:
			nextActivityIntent = new Intent(this, AnimationActivity.class);
			break;
		case LEVEL_NORMAL:
			nextActivityIntent = new Intent(this, ComplainActivity.class);
			break;
		case LEVEL_HARD:
			nextActivityIntent = new Intent(this, ForPeopleActivity.class);
			break;
		}
		return nextActivityIntent;
	}

}