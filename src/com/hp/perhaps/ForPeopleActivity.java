package com.hp.perhaps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class ForPeopleActivity extends Activity implements OnClickListener {

	TextView txtPlease;
	ImageButton btnVot;
	EditText txtForPeople;
	

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_for_text);

		txtPlease = (TextView) findViewById(R.id.txtPlease);
		txtForPeople = (EditText) findViewById(R.id.txtForPeople);
		btnVot = (ImageButton) findViewById(R.id.btnVot);
		btnVot.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnVot:
			startActivity(new Intent(ForPeopleActivity.this,
					FromPeoplesActivity.class));
			finish();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_complain, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		switch (item.getItemId()) {

		case R.id.level:
			startActivity(new Intent(this, LevelActivity.class));
			return true;

		case R.id.question:
			Toast.makeText(this, "Все будет хорошо =)", Toast.LENGTH_LONG)
					.show();
			return true;

		case R.id.about:
			startActivity(new Intent(this, AboutActivity.class));
			return true;

		default:
			return super.onMenuItemSelected(featureId, item);
		}
	}

}
