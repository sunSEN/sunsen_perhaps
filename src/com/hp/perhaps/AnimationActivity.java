package com.hp.perhaps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class AnimationActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_animation);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_complain, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		switch (item.getItemId()) {

		case R.id.level:
			startActivity(new Intent(this, LevelActivity.class));
			return true;

//		case R.id.play:
//			Toast.makeText(this, "Go animation", Toast.LENGTH_LONG).show();
//			return true;

		case R.id.question:
			Toast.makeText(this, "Все будет хорошо =)", Toast.LENGTH_LONG)
					.show();
			return true;

		case R.id.about:
			startActivity(new Intent(this, AboutActivity.class));
			return true;

		default:
			return super.onMenuItemSelected(featureId, item);
		}
	}
}
