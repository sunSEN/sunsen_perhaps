package com.hp.perhaps;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class AboutActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_author);

		TextView txtAuthor = (TextView) findViewById(R.id.textAuthor);
		String app_ver = "";
		try {
			app_ver = this.getPackageManager().getPackageInfo(
					this.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			Log.v(AboutActivity.class.getSimpleName(), e.getMessage());
		}
		txtAuthor.setText(getString(R.string.txtAuthor) + " " + app_ver);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_complain, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		switch (item.getItemId()) {

		case R.id.level:
			startActivity(new Intent(this, LevelActivity.class));
			return true;

			// case R.id.play:
			// Toast.makeText(this, "Go animation", Toast.LENGTH_LONG).show();
			// return true;

		case R.id.question:
			Toast.makeText(this, "Все будет хорошо =)", Toast.LENGTH_LONG)
					.show();
			return true;

		default:
			return super.onMenuItemSelected(featureId, item);
		}
	}
}
