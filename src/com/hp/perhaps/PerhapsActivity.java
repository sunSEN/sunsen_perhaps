package com.hp.perhaps;

import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class PerhapsActivity extends Activity implements OnClickListener {

	String[] mWords = { "Ты ж мой бедный зайчик =)", "Ты ж мое солнышко =)",
			"Держись, все пройдет", "Хорошенькая, все будет хорошо",
			"Ты не один, 1+1 =)", "Не падай духом, ушибешся =)",
			"Моя милая, все пройдет", "Все будет офигенно",
			"Котёночек, они все такие" };

	TextView txtMain;
	ImageButton btnMore;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_perhaps);

		txtMain = (TextView) findViewById(R.id.txtMain);

		Random numW = new Random();
		int numberWord = numW.nextInt(mWords.length);
		txtMain.setText(mWords[numberWord]);

		btnMore = (ImageButton) findViewById(R.id.btnMore);
		btnMore.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_complain, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		switch (item.getItemId()) {

		case R.id.level:
			startActivity(new Intent(this, LevelActivity.class));
			finish();
			return true;

			// case R.id.play:
			// Toast.makeText(this, "Go animation", Toast.LENGTH_LONG).show();
			// return true;

		case R.id.question:
			Toast.makeText(this, "Все будет хорошо =)", Toast.LENGTH_LONG)
					.show();
			return true;

		case R.id.about:
			startActivity(new Intent(this, AboutActivity.class));
			return true;

		default:
			return super.onMenuItemSelected(featureId, item);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnMore:

			Random numW = new Random();
			int numberWord = numW.nextInt(mWords.length);
			txtMain.setText(mWords[numberWord]);

			break;
		}
	}
}
